import java.util.Date;

public class NQueensCSP extends CSP{
	
	NQueensDomain domain;
	
	public NQueensCSP(int N){
		super();
		domain = new NQueensDomain(N);
		for (int i = 1; i <= N; i++) {
			Variable queen = new Variable(Integer.toString(i), domain);
			this.variables.add(queen);
		}
		for (int i = 0; i < N; i++) {
			for (int j = i + 1; j < N; j++) {
				Variable v1 = this.variables.get(i);
				Variable v2 = this.variables.get(j);
				
				this.constraints.add(new NQueenConstraint(v1, v2, N));
			}
		}

	}
	public static void main(String[] args) {
		System.out.println("NQueens Problem (AIMA and Prof. Ferguson)");
		CSP csp = new NQueensCSP(8);
		System.out.println(csp);
		System.out.println("Backtracking search solver");
		Backtrack solver = new Backtrack();
		
		long start = new Date().getTime();
		System.out.println("8 Queens \ncolumn: c " + " " + "row: r ");
		System.out.println("c" + "   " + "r");
		Assignment result = solver.backtrackingSearch(csp);
		System.out.println(result.toString());
		System.out.println("stopped");
		long end = new Date().getTime();
		System.out.format("time: %.3f secs\n", (end-start)/1000.0);
		System.out.println("result=" + result);
	}

}
