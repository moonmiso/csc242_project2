CSC242_Proj2
Name: Muskaan Mendiratta
Assignment Number: Project 02, Unit 02
I did not collaborate with anyone on this project.

Files:
CSP FORMALIZATION AND GENERALIZED ALGORITHM
Assignment.java
Value.java
Variable.java
Domain.java
Constraint.java
CSP.java
Backtrack.java

AUSTRALIA MAP
AustraliaCSP.java		(main method for running Australia CSP)
AustraliaDomain.java
NotEqualConstraint.java

JOP SHOP 
JobShopCSP.java			(main method for running Job Shop CSP)
JobShopDomain.java
DisjunctiveConstraint.java
PrecedenceConstraint.java

N QUEENS
NQueens.java
NQueensCSP.java			(main method for running NQueens CSP)
NQueensDomain.java
NQueensConstrait.java

Compiling and executing instructions:
This project has been written in Java.
Steps for compiling and executing:
javac *.java
java AustraliaCSP
java JobShopCSP
java NQueensCSP

On the console:
I used the main method provided by Prof. Ferguson. Job shop takes about 3 seconds to print results.



