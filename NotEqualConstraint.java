import java.util.ArrayList;

public class NotEqualConstraint extends Constraint{

	Variable var1 = new Variable();
	Variable var2 = new Variable();

	public NotEqualConstraint(Variable v1,Variable v2) {
		var1 = v1;
		var2 = v2;
	}

	@Override
	public boolean isSatisfiedWith(Assignment assignment) {
		Value v1 = assignment.get(var1);
		Value v2 = assignment.get(var2);

		if(v1 == null || v2 == null) {
			return true;
		}

		if (!v1.toString().equals(v2.toString())) {
			return true;
		}

		return false;
	}
}
