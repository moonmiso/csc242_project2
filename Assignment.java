import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class Assignment {
	Map<Variable, Value> tuple = new HashMap<>();
	public Assignment() {		
	}	
	public Assignment(Variable v, Value value) {
		v.value = value;
		tuple.put(v, value);
	}
	
	public Value get(Variable v) {
		return tuple.get(v);
	}
	
	public void put (Variable v, Value value) {
		tuple.put(v, value);
	}
	
	public boolean isConsistent(ArrayList<Constraint> constraints) {
		for (Constraint c : constraints) {
			if (!c.isSatisfiedWith(this)) {
				return false;
			}
		}

		return true;
	}
	

	
}
