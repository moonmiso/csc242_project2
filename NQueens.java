
public class NQueens {
	int row;
	int column;
	int N;
	int lowerRight;
	int upperRight;

	public NQueens(int N, int row, int column) {
		this.row = row;
		this.column = column;
		this.N = N;
		this.lowerRight = row + column;
		this.upperRight = row - column + 1;
	}

	
}
