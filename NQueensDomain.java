import java.util.ArrayList;

public class NQueensDomain extends Domain{
	int N;

	public NQueensDomain(int N) {
		this.N = N;
		for (int i = 1; i <= N; i++) {

			this.domain.add(new Value(i));
			}
	}

}
