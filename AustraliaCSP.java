import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;

public class AustraliaCSP extends CSP{
	public AustraliaCSP() {
		this.variables = new ArrayList<Variable>();
		this.constraints = new ArrayList<Constraint>();

		AustraliaDomain rgb = new AustraliaDomain();
		Variable WA = new Variable("WA", rgb);
		this.variables.add(WA);
		Variable NT = new Variable("NT", rgb);
		this.variables.add(NT);
		Variable Q = new Variable("Q", rgb);
		this.variables.add(Q);
		Variable NSW = new Variable("NSW", rgb);
		this.variables.add(NSW);
		Variable V = new Variable("V", rgb);
		this.variables.add(V);
		Variable SA = new Variable("SA", rgb);
		this.variables.add(SA);
		Variable T = new Variable("T", rgb);
		this.variables.add(T);
		this.constraints.add(new NotEqualConstraint(SA, WA));
		this.constraints.add(new NotEqualConstraint(SA, NT));
		this.constraints.add(new NotEqualConstraint(SA, Q));
		this.constraints.add(new NotEqualConstraint(SA, NSW));
		this.constraints.add(new NotEqualConstraint(SA, V));
		this.constraints.add(new NotEqualConstraint(WA, NT));
		this.constraints.add(new NotEqualConstraint(NT, Q));
		this.constraints.add(new NotEqualConstraint(Q, NSW));
		this.constraints.add(new NotEqualConstraint(NSW, V));
	}
	
	public static void main(String[] args) {
		System.out.println("Australia Map Coloring Problem (AIMA 6.1.1)");
		CSP csp = new AustraliaCSP();
		System.out.println(csp);
		System.out.println("Backtracking search solver");
		Backtrack solver = new Backtrack();
		
		long start = new Date().getTime();
		Assignment result = solver.backtrackingSearch(csp);
		System.out.println(result.toString());
		System.out.println("stopped");
		long end = new Date().getTime();
		System.out.format("time: %.3f secs\n", (end-start)/1000.0);
		System.out.println("result=" + result);
	}

}
