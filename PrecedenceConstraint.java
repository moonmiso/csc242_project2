
public class PrecedenceConstraint extends Constraint{
	Variable var1 = new Variable();
	Variable var2 = new Variable();
	int minutes;

	public PrecedenceConstraint(Variable v1, int min, Variable v2) {
		var1 = v1;
		var2 = v2;
		minutes = min;
	}

	@Override
	public boolean isSatisfiedWith(Assignment assignment) {
		Value v1 = assignment.get(var1);
		Value v2 = assignment.get(var2);

		if(v1 == null || v2 == null) {
			return true;
		}

		if (((Integer.parseInt(v1.value.toString()) + minutes) <= Integer.parseInt(v2.value.toString()))) {
			return true;
		}

		return false;
	}

}
