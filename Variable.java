import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Variable {
	Domain domain;
	Value value;
	String string = "";
	
	//ArrayList<Domain> domain;
	public Variable() {
		this.value = new Value("");

	}
	public Variable(String string, Domain domain) {
		this.string = string;
		this.domain = domain;
		this.value = new Value("");
	}
}
