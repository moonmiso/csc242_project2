
public class Value<E> {
	E value;
	public Value(E value) {
		this.value = value;
	}
	public String toString() {
		return this.value.toString();
	}
}
