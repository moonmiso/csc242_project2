import java.util.ArrayList;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;

public class CSP {
	ArrayList<Variable> variables;
	ArrayList<Constraint> constraints;
	Constraint constraint;	
	Hashtable<Variable, ArrayList<Constraint>> cnet;
	public CSP() {
		//set variables 
		this.variables = new ArrayList<Variable>();
		this.constraints = new ArrayList<Constraint>();
	}
	
	public Variable selectUnassignedVariable(Assignment assignment, CSP csp) {
		for (Variable v : csp.variables) {
			if (assignment.tuple.get(v) == null) {
				return v;
			}
		}
		return null;
	}


	public Variable assignmentComplete(Assignment a) {
		for (Variable v : this.variables) {
			if (a.tuple.get(v)==null) {
				return v;
			}
		}
		toString(this);
		System.out.println("assignment complete");
		return null;
	}
	public void toString(CSP csp) {
		for (Variable v : csp.variables) {

			System.out.println(v.string + "   " + v.value.toString());
		}
		System.out.println();
	}

}
