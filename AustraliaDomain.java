
public class AustraliaDomain extends Domain{
	public AustraliaDomain() {
		Value red = new Value("red");
		Value green = new Value("green");
		Value blue = new Value("blue");
		this.domain.add(red);
		this.domain.add(blue);
		this.domain.add(green);
	}

}
