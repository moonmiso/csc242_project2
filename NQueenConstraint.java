
public class NQueenConstraint extends Constraint{
	int N;
	Variable var1 = new Variable();
	Variable var2 = new Variable();

	public NQueenConstraint(Variable var1, Variable var2, int N) {
		this.var1 = var1;
		this.var2 = var2;
		this.N = N;
	}
	void printSolution(int board[][]) {
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				System.out.print(" " + board[i][j] + " ");
			}
			System.out.println();
		}
	}

	public boolean isSatisfiedWith(Assignment assignment) {
		Value value1 = assignment.get(var1);
		Value value2 = assignment.get(var2);
		if(value1 == null || value2 == null) {
			return true;
		}
		int row1 = Integer.parseInt(value1.toString());	//column
		int row2 = Integer.parseInt(value2.toString());
		int col1 = Integer.parseInt(var1.string.toString());
		int col2 = Integer.parseInt(var2.string.toString());
		NQueens position1 = new NQueens(N, row1, col1);
		NQueens position2 = new NQueens(N, row2, col2);		

		if (!(row1 == row2 || col1 == col2 || 
				position1.lowerRight == position2.lowerRight||
				position1.upperRight == position2.upperRight)) {
			return true;
		}
		return false;
	}


}
