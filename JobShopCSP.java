import java.util.Date;

public class JobShopCSP extends CSP{
	JobShopDomain domain;
	//X = {Axle F, Axle B, Wheel RF ,Wheel LF ,Wheel RB, Wheel LB, Nuts RF, 
	//	   Nuts LF, Nuts RB, Nuts LB, Cap RF, Cap LF ,Cap RB, Cap LB, Inspect}
	
	public JobShopCSP() {
		super();
		domain = new JobShopDomain();
		Variable Inspect = new JobShopVariable("Inspect", domain);
		this.variables.add(Inspect);	

		Variable AxleF = new JobShopVariable("AxleF", domain);
		Variable AxleB = new JobShopVariable("AxleB", domain);
		Variable WheelRF = new JobShopVariable("Wheel RF", domain);
		Variable WheelLF = new JobShopVariable("Wheel LF", domain);
		Variable WheelRB = new JobShopVariable("Wheel RB", domain);
		Variable WheelLB = new JobShopVariable("Wheel LB", domain);
		Variable NutsRF = new JobShopVariable("Nuts RF", domain);
		Variable NutsLF = new JobShopVariable("Nuts LF", domain);
		Variable NutsRB = new JobShopVariable("Nuts RB", domain);
		Variable NutsLB = new JobShopVariable("Nuts LB", domain);
		Variable CapRF = new JobShopVariable("Cap RF", domain);
		Variable CapLF = new JobShopVariable("Cap LF", domain);
		Variable CapRB = new JobShopVariable("Cap RB", domain);
		Variable CapLB = new JobShopVariable("Cap LB", domain);

		this.variables.add(AxleF);
		this.variables.add(AxleB);
		this.variables.add(WheelRF);
		this.variables.add(WheelLF);
		this.variables.add(WheelRB);
		this.variables.add(WheelLB);
		this.variables.add(NutsRF);
		this.variables.add(NutsLF);
		this.variables.add(NutsRB);
		this.variables.add(NutsLB);
		this.variables.add(CapRF);
		this.variables.add(CapLF);
		this.variables.add(CapRB);
		this.variables.add(CapLB);
		
		/*
	
		 * (AxleF + 10 <= AxleB) or (AxleB + 10 <= AxleF ) 
		*/
		this.constraints.add(new DisjunctiveConstraint(AxleF, 10, AxleB));	

		this.constraints.add(new PrecedenceConstraint(AxleF, 10, WheelRF));
		this.constraints.add(new PrecedenceConstraint(AxleF, 10, WheelLF));
		this.constraints.add(new PrecedenceConstraint(AxleB, 10, WheelRB));
		this.constraints.add(new PrecedenceConstraint(AxleB, 10, WheelLB));
		this.constraints.add(new PrecedenceConstraint(WheelRF, 1, NutsRF));
		this.constraints.add(new PrecedenceConstraint(NutsRF, 2, CapRF));
		this.constraints.add(new PrecedenceConstraint(WheelLF, 1, NutsLF));
		this.constraints.add(new PrecedenceConstraint(NutsLF, 2, CapLF));
		this.constraints.add(new PrecedenceConstraint(WheelRB, 1, NutsRB));
		this.constraints.add(new PrecedenceConstraint(NutsRB, 2, CapLF));
		this.constraints.add(new PrecedenceConstraint(WheelLB, 1, NutsLB));
		this.constraints.add(new PrecedenceConstraint(NutsLB, 2, CapLB));
		
		this.constraints.add(new PrecedenceConstraint(AxleF, 10, Inspect));
		this.constraints.add(new PrecedenceConstraint(AxleF, 10, Inspect));
		this.constraints.add(new PrecedenceConstraint(AxleB, 10, Inspect));
		this.constraints.add(new PrecedenceConstraint(AxleB, 10, Inspect));
		this.constraints.add(new PrecedenceConstraint(WheelRF, 1, Inspect));
		this.constraints.add(new PrecedenceConstraint(NutsRF, 2, Inspect));
		this.constraints.add(new PrecedenceConstraint(WheelLF, 1, Inspect));
		this.constraints.add(new PrecedenceConstraint(NutsLF, 2, Inspect));
		this.constraints.add(new PrecedenceConstraint(WheelRB, 1, Inspect));
		this.constraints.add(new PrecedenceConstraint(NutsRB, 2, Inspect));
		this.constraints.add(new PrecedenceConstraint(WheelLB, 1, Inspect));
		this.constraints.add(new PrecedenceConstraint(NutsLB, 2, Inspect));
		this.constraints.add(new PrecedenceConstraint(CapRF, 1, Inspect));
		this.constraints.add(new PrecedenceConstraint(CapLF, 1, Inspect));
		this.constraints.add(new PrecedenceConstraint(CapRB, 1, Inspect));
		this.constraints.add(new PrecedenceConstraint(CapLB, 1, Inspect));
		
	}
	public static void main(String[] args) {
		System.out.println("Job Shop Problem (AIMA 6.1.2)");
		CSP csp = new JobShopCSP();
		System.out.println(csp);
		System.out.println("Backtracking search solver");
		Backtrack solver = new Backtrack();
		
		long start = new Date().getTime();
		Assignment result = solver.backtrackingSearch(csp);
		//System.out.println(result.toString());
		System.out.println("stopped");
		long end = new Date().getTime();
		System.out.format("time: %.3f secs\n", (end-start)/1000.0);
		System.out.println("result=" + result);
	}

}
