import java.util.LinkedList;
import java.util.Queue;

public class Backtrack {
	public Assignment backtrackingSearch(CSP csp) {
		Assignment a = new Assignment();
		return backtrackSolver(a, csp);
	}
	public Assignment backtrackSolver(Assignment a, CSP csp) {
		Assignment failure = null;	//failure is a null assignment
		//if assignment is complete then return assignment
		Variable v = csp.assignmentComplete(a);
		if (v == null) {
			return a;
		}		
		//var = SELECT-UNASSIGNED-VARIABLE(csp)
//		Variable v = csp.selectUnassignedVariable(a, csp);
		//for each value in ORDER-DOMAIN-VALUES(var, assignment, csp) 
		for (Value d : v.domain.domain) {	//list of values in the domain of the variable
			//if value is consistent with assignment 
			a.tuple.put(v, d);
			v.value = d;

			if (a.isConsistent(csp.constraints)) {
				//add {var = value} to assignment
				Assignment result = backtrackSolver(a, csp);
				if (result != (failure)) {
					return result;
				} else {
					a.tuple.remove(v);
					v.value = null;
				}
				
			}else {
				a.tuple.remove(v);
				v.value = null;
			}

		}
		return failure;

	}
}



